package sc.maven.springboottutorial.database.department

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.SequenceGenerator
import jakarta.persistence.Table

@Entity
@Table(name = "department")
class DepartmentEntity (
    @Id
    @SequenceGenerator(name = "department_id_seq_generator", sequenceName = "department_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_id_seq_generator")
    var departmentId: Long = 0L,

    var departmentName: String = "",
    var departmentAddress: String = "",
    var departmentCode: String = ""
)