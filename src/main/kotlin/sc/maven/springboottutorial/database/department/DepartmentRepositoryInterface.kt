package sc.maven.springboottutorial.database.department

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DepartmentRepositoryInterface : JpaRepository<DepartmentEntity, Long>