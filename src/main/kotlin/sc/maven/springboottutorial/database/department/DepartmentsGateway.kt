package sc.maven.springboottutorial.database.department

import org.springframework.stereotype.Repository
import sc.maven.springboottutorial.domain.department.Department
import sc.maven.springboottutorial.domain.department.DepartmentInfo
import sc.maven.springboottutorial.domain.department.DepartmentsInterface
import java.util.*

@Repository
class DepartmentsGateway (
    private val departmentRepository: DepartmentRepositoryInterface,
    private val departmentMapper: DepartmentDatabaseMapper
) : DepartmentsInterface {
    override fun saveDepartment(department: Department) {
        val departmentEntity = departmentMapper.mapDepartmentEntityFrom(department)
        departmentRepository.save(departmentEntity)
    }

    override fun getAllDepartments(): List<Department> {
        val departmentEntities = departmentRepository.findAll()
        val departments = departmentEntities.map {
            departmentEntity -> departmentMapper.mapDepartmentFrom(departmentEntity)
        }
        return departments
    }

    override fun getDepartment(id: Long): Optional<Department> {
        val departmentOptional = departmentRepository.findById(id)

        if (departmentOptional.isEmpty) {
            return Optional.empty()
        }

        val department = departmentMapper.mapDepartmentFrom(departmentOptional.get())
        return Optional.of(department)
    }

    override fun deleteDepartment(id: Long) {
        departmentRepository.deleteById(id)
    }

    override fun updateDepartment(id: Long, departmentInfo: DepartmentInfo) {
        val departmentEntityOptional = departmentRepository.findById(id)

        if (departmentEntityOptional.isPresent) {
            val existingDepartmentEntity = departmentEntityOptional.get()

            if (valueIsSet(departmentInfo.departmentName)) {
                existingDepartmentEntity.departmentName = departmentInfo.departmentName
            }

            if (valueIsSet(departmentInfo.departmentAddress)) {
                existingDepartmentEntity.departmentAddress = departmentInfo.departmentAddress
            }

            if (valueIsSet(departmentInfo.departmentCode)) {
                existingDepartmentEntity.departmentCode = departmentInfo.departmentCode
            }

            departmentRepository.save(existingDepartmentEntity)
        }
    }

    private fun valueIsSet(attribute: String): Boolean {
        return attribute != ""
    }
}