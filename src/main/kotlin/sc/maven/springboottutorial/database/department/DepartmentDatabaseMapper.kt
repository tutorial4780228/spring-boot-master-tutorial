package sc.maven.springboottutorial.database.department

import org.springframework.stereotype.Service
import sc.maven.springboottutorial.domain.department.Department
import sc.maven.springboottutorial.database.department.DepartmentEntity as DepartmentEntity

@Service
class DepartmentDatabaseMapper {
    fun mapDepartmentEntityFrom(department: Department): DepartmentEntity {
        return DepartmentEntity(
            departmentName = department.departmentName,
            departmentCode = department.departmentCode,
            departmentAddress = department.departmentAddress
        )
    }

    fun mapDepartmentFrom(departmentEntity: DepartmentEntity): Department {
        return Department(
            departmentId = departmentEntity.departmentId,
            departmentName = departmentEntity.departmentName,
            departmentAddress = departmentEntity.departmentAddress,
            departmentCode = departmentEntity.departmentCode
        )
    }
}