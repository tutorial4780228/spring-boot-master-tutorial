package sc.maven.springboottutorial.domain.department

import org.springframework.stereotype.Service
import sc.maven.springboottutorial.domain.department.exception.InvalidDepartmentException
import sc.maven.springboottutorial.domain.department.exception.DepartmentDoesNotExistException

@Service
class DepartmentService (
    private val departments: DepartmentsInterface,
) : DepartmentServiceInterface {
    override fun saveNewDepartment(department: Department) {
        verifyDepartment(department)
        departments.saveDepartment(department)
    }

    override fun getAllDepartments(): List<Department> {
        return departments.getAllDepartments()
    }

    override fun getDepartment(id: Long): Department {
        val departmentOptional = departments.getDepartment(id)

        if (departmentOptional.isEmpty) {
            throw DepartmentDoesNotExistException("Department #$id was not found")
        }

        return departmentOptional.get()
    }

    override fun deleteDepartment(id: Long) {
        departments.deleteDepartment(id)
    }

    override fun updateDepartment(id: Long, departmentInfo: DepartmentInfo) {
        departments.updateDepartment(id, departmentInfo)
    }

    private fun verifyDepartment(department: Department) {
        val departmentIsValid = department.departmentCode.contains('-')
        if (!departmentIsValid) {
            throw InvalidDepartmentException("Department code is not valid")
        }
    }
}