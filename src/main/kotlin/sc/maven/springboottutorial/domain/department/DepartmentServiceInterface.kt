package sc.maven.springboottutorial.domain.department

interface DepartmentServiceInterface {
    fun saveNewDepartment(department: Department)
    fun getAllDepartments(): List<Department>
    fun getDepartment(id: Long): Department
    fun deleteDepartment(id: Long)
    fun updateDepartment(id: Long, departmentInfo: DepartmentInfo)
}