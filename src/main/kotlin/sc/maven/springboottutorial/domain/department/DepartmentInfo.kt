package sc.maven.springboottutorial.domain.department

class DepartmentInfo(
    var departmentName: String = "",
    var departmentAddress: String = "",
    var departmentCode: String = ""
)