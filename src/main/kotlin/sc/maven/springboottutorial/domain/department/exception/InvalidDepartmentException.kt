package sc.maven.springboottutorial.domain.department.exception

class InvalidDepartmentException(
    override val message: String?
) : RuntimeException(message)