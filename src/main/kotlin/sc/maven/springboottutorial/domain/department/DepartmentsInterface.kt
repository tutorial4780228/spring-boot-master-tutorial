package sc.maven.springboottutorial.domain.department

import java.util.*

interface DepartmentsInterface {
    fun saveDepartment(department: Department)
    fun getAllDepartments(): List<Department>
    fun getDepartment(id: Long): Optional<Department>
    fun deleteDepartment(id: Long)
    fun updateDepartment(id: Long, departmentInfo: DepartmentInfo)
}