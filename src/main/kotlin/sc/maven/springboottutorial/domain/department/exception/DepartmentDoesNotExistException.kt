package sc.maven.springboottutorial.domain.department.exception

class DepartmentDoesNotExistException(
    override val message: String?
) : RuntimeException(message)