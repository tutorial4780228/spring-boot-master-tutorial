package sc.maven.springboottutorial.domain.department

class Department (
    var departmentId: Long = 0L,
    var departmentName: String,
    var departmentAddress: String,
    var departmentCode: String
)