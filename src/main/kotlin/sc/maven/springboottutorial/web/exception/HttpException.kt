package sc.maven.springboottutorial.web.exception

import org.springframework.http.HttpStatus

open class HttpException(
    override val message: String?,
    open val httpStatus: HttpStatus
) : RuntimeException(message)