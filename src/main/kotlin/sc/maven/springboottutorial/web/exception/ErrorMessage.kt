package sc.maven.springboottutorial.web.exception

import org.springframework.http.HttpStatus

class ErrorMessage (
    var status: HttpStatus,
    var message: String?
)