package sc.maven.springboottutorial.web.exception

import org.springframework.http.HttpStatus

class ValidationErrorMessage(
    var status: HttpStatus,
    var errorMessages: List<String?> = emptyList()
)