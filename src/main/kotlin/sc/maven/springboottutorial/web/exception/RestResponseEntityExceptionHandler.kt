package sc.maven.springboottutorial.web.exception

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import sc.maven.springboottutorial.web.department.DepartmentController

@ControllerAdvice(assignableTypes = [DepartmentController::class])
@ResponseStatus
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(HttpException::class)
    fun httpException(exception: HttpException, webRequest: WebRequest): ResponseEntity<ErrorMessage> {
        val errorMessage = ErrorMessage(exception.httpStatus, exception.message)
        return ResponseEntity.status(exception.httpStatus).body(errorMessage)
    }

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatusCode,
        request: WebRequest
    ): ResponseEntity<Any>? {
        val fieldErrors = ex.bindingResult.fieldErrors
        val errorMessages = fieldErrors.map {
            fieldError -> "${fieldError.field}: ${fieldError.defaultMessage}"
        }
        val errorMessage = ValidationErrorMessage(HttpStatus.UNPROCESSABLE_ENTITY, errorMessages)
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMessage)
    }
 }