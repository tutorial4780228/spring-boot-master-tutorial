package sc.maven.springboottutorial.web.department.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import sc.maven.springboottutorial.web.exception.HttpException

class InvalidDepartmentHttpException(
    override val message: String?,
    override val httpStatus: HttpStatus = HttpStatus.UNPROCESSABLE_ENTITY
) : HttpException(message, httpStatus)