package sc.maven.springboottutorial.web.department.dto

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import org.hibernate.validator.constraints.Length

class CreateDepartmentDTO (
    @field: NotBlank
    @field: Length(min = 2, message = "Department name is too short")
    var departmentName: String,
    var departmentAddress: String,
    @field: Pattern(regexp = "(^\\w{1,5}-\\d{3}\$)?", message = "Department code must look like 'IT-001'")
    var departmentCode: String
)