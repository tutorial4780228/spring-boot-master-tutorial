package sc.maven.springboottutorial.web.department.util

import org.springframework.stereotype.Service
import sc.maven.springboottutorial.domain.department.Department
import sc.maven.springboottutorial.domain.department.DepartmentInfo
import sc.maven.springboottutorial.web.department.dto.CreateDepartmentDTO
import sc.maven.springboottutorial.web.department.dto.GetDepartmentDTO
import sc.maven.springboottutorial.web.department.dto.UpdateDepartmentDTO

@Service
class DepartmentWebMapper {
    fun mapDepartmentFrom(createDepartmentDTO: CreateDepartmentDTO): Department {
        return Department(
            departmentName = createDepartmentDTO.departmentName,
            departmentAddress = createDepartmentDTO.departmentAddress,
            departmentCode = createDepartmentDTO.departmentCode
        )
    }

    fun mapDepartmentInfoFrom(updateDepartmentDTO: UpdateDepartmentDTO): DepartmentInfo {
        return DepartmentInfo(
            departmentName = updateDepartmentDTO.departmentName,
            departmentAddress = updateDepartmentDTO.departmentAddress,
            departmentCode = updateDepartmentDTO.departmentCode
        )
    }

    fun mapGetDepartmentDtoFrom(department: Department): GetDepartmentDTO {
        return GetDepartmentDTO(
            departmentName = department.departmentName,
            departmentCode = department.departmentCode,
            departmentAddress = department.departmentAddress,
            departmentId = department.departmentId
        )
    }

    fun mapGetDepartmentsDtoFrom(departments: List<Department>): List<GetDepartmentDTO> {
        return departments.map {
                department ->  mapGetDepartmentDtoFrom(department)
            }
    }
}