package sc.maven.springboottutorial.web.department.dto

class GetDepartmentDTO (
    var departmentId: Long,
    var departmentName: String,
    var departmentAddress: String,
    var departmentCode: String
)