package sc.maven.springboottutorial.web.department

import jakarta.validation.Valid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import sc.maven.springboottutorial.domain.department.DepartmentServiceInterface
import sc.maven.springboottutorial.domain.department.exception.DepartmentDoesNotExistException
import sc.maven.springboottutorial.domain.department.exception.InvalidDepartmentException
import sc.maven.springboottutorial.web.department.dto.CreateDepartmentDTO
import sc.maven.springboottutorial.web.department.dto.GetDepartmentDTO
import sc.maven.springboottutorial.web.department.dto.UpdateDepartmentDTO
import sc.maven.springboottutorial.web.department.exception.DepartmentNotFoundHttpException
import sc.maven.springboottutorial.web.department.exception.InvalidDepartmentHttpException
import sc.maven.springboottutorial.web.department.util.DepartmentWebMapper

@RestController
@RequestMapping("/department")
class DepartmentController (
    private val departmentService: DepartmentServiceInterface,
    private val departmentMapper: DepartmentWebMapper,
) {
    private val logger: Logger = LoggerFactory.getLogger(DepartmentController::class.java)

    @PostMapping
    fun saveNewDepartment(@Valid @RequestBody createDepartmentDTO: CreateDepartmentDTO) {
        logger.info("Saving department ${createDepartmentDTO.departmentName} (${createDepartmentDTO.departmentCode})")
        val department = departmentMapper.mapDepartmentFrom(createDepartmentDTO)

        try {
            departmentService.saveNewDepartment(department)
            logger.info("Saving department ${createDepartmentDTO.departmentName} (${createDepartmentDTO.departmentCode}) " +
                    "was successful")
        } catch (invalidDepartmentException: InvalidDepartmentException) {
            logger.error("Saving department ${createDepartmentDTO.departmentName} (${createDepartmentDTO.departmentCode}) " +
                    "failed")
            throw InvalidDepartmentHttpException(invalidDepartmentException.message)
        }
    }

    @GetMapping
    fun getAllDepartments(): List<GetDepartmentDTO> {
        val departments = departmentService.getAllDepartments()
        val departmentsDto = departmentMapper.mapGetDepartmentsDtoFrom(departments)
        return departmentsDto
    }

    @GetMapping("/{id}")
    fun getDepartmentWithIdOf(@PathVariable id: Long): GetDepartmentDTO {
        try {
            val department = departmentService.getDepartment(id)
            val departmentDto = departmentMapper.mapGetDepartmentDtoFrom(department)
            return departmentDto
        } catch (departmentDoesNotExistException: DepartmentDoesNotExistException) {
            throw DepartmentNotFoundHttpException(departmentDoesNotExistException.message)
        }
    }

    @DeleteMapping("/{id}")
    fun deleteDepartmentWithIdOf(@PathVariable id: Long) {
        departmentService.deleteDepartment(id)
    }

    @RequestMapping(method = [RequestMethod.PUT, RequestMethod.PATCH], path = ["/{id}"])
    fun updateDepartmentWithIdOf(
        @PathVariable id: Long,
        @Valid @RequestBody updateDepartmentDTO: UpdateDepartmentDTO
    ) {
        val updatedDepartment = departmentMapper.mapDepartmentInfoFrom(updateDepartmentDTO)
        departmentService.updateDepartment(id, updatedDepartment)
    }
}