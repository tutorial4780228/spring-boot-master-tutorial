package sc.maven.springboottutorial.web.department.exception

import org.springframework.http.HttpStatus
import sc.maven.springboottutorial.web.exception.HttpException

class DepartmentNotFoundHttpException(
    override val message: String?,
    override val httpStatus: HttpStatus = HttpStatus.NOT_FOUND
) : HttpException(message, httpStatus)