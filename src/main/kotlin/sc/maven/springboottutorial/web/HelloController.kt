package sc.maven.springboottutorial.web

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {
    @Value("\${system.messages.welcome-message}")
    private lateinit var welcomeMessage: String

    @GetMapping("/")
    fun getHtml(): String {
        return welcomeMessage
    }
}