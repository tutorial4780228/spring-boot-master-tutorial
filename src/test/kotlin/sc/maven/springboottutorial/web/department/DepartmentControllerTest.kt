package sc.maven.springboottutorial.web.department

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import sc.maven.springboottutorial.domain.department.Department
import sc.maven.springboottutorial.domain.department.DepartmentServiceInterface

@AutoConfigureMockMvc
@SpringBootTest
class DepartmentControllerTest (
    @Autowired private val mockMvcTest: MockMvc,
) {
    @MockBean
    private lateinit var departmentService: DepartmentServiceInterface

    @Test
    fun saveDepartment() {
        val department = Department(
            departmentName = "IT",
            departmentAddress = "800 Big Ave",
            departmentCode = "IT-003"
        )

        Mockito.doNothing().`when`(departmentService).saveNewDepartment(department)
        mockMvcTest.perform(
            MockMvcRequestBuilders.post("/department")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {
                    	"departmentName": "IT",
                    	"departmentCode": "IT-003",
                    	"departmentAddress": "800 Big Ave"
                    }
                """.trimIndent())
        ).andExpect(MockMvcResultMatchers.status().isOk)
    }
}