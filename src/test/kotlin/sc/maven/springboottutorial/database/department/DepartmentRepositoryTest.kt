package sc.maven.springboottutorial.database.department

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import sc.maven.springboottutorial.domain.department.Department

@DataJpaTest
class DepartmentRepositoryTest (
    @Autowired private val departmentRepository: DepartmentRepositoryInterface,
    @Autowired private val testEntityManager: TestEntityManager
) {
    @BeforeEach
    fun setUp() {
        val department = Department(
            departmentName = "Mechanical Engineering",
            departmentCode = "ME-011",
            departmentAddress = "777 Big Ave"
        )

        testEntityManager.persist(department)
    }

    @Test
    fun `when find by id is called then return department`() {
        val department = departmentRepository.findById(1L).get()
        assert(department.departmentName == "ME-011")
    }
}