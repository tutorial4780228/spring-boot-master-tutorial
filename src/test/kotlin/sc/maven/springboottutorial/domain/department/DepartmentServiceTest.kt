package sc.maven.springboottutorial.domain.department

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

@SpringBootTest
class DepartmentServiceTest (
    @Autowired private val departmentService: DepartmentService,
) {
    @MockBean
    private lateinit var departments: DepartmentsInterface
    private val validDepartmentId: Long = 1L

    @BeforeEach
    fun setUp() {
        val department = Department(
            departmentId = validDepartmentId,
            departmentName = "IT Department #1",
            departmentCode = "IT-001",
            departmentAddress = "800 Highway Street"
        )

        Mockito.`when`(departments.getDepartment(validDepartmentId))
            .thenReturn(Optional.of(department))
    }

    @Test
    @DisplayName("When valid department id is given should return department")
    fun `When valid department id is given should return department`() {
        val department = departmentService.getDepartment(validDepartmentId)

        assert(department.departmentId == validDepartmentId)
    }
}